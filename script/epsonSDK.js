$( function(){
	// setInterval( "makeTable()", 2000 );
	// makeTable();
} );

function makeTable( $stts ) {
	$.ajax({
		url: "./lookDB.php",
		type: "POST",
		// data: { val : $keyword }, // 検索など引数を渡す必要があるときこれを使う。受け取りは$_POST['val']的な。
		// dataType: 'json',        // サーバーなどの環境によってこのオプションが必要なときがある
		success: function( $arr ){
			var $parseAr = JSON.parse( $arr );
			// var $id = $parseAr[0]['id'].toString();
			$html = "<table><tr><th>番号</th><th>CG</th><th>人数</th><th>状態</th><th>受付け</th><th>来た</th><th>撮影終了</th></tr>";
			for (var $i = $parseAr.length-1; $i >= 0; $i--) {
				$html += "<tr>";
				$html += "<td>"+ $parseAr[$i]['id'].toString() +"</td>";
				$html += "<td>"+ $parseAr[$i]['cg'].toString() +"</td>";
				$html += "<td>"+ $parseAr[$i]['num'].toString() +"</td>";
				if ($parseAr[$i]['status'].toString().length > 2) {
					$html += '<td style="background-color:'+[
				"#ffdddd", //赤
				"#ffffdd", //黄
				"#ddffdd", //緑
			][Number($parseAr[$i]['status'].toString()[$parseAr[$i]['status'].toString().length-1])]+';">'
				} else {
					$html += "<td>"
				}
				$html += ($parseAr[$i]['status'].toString()).replace(/[0-9]/, "") +"</td>";
				$html += "<td>"+ $parseAr[$i]['recepttime'].toString() +"</td>";
				if ($parseAr[$i]['cometime'].toString().length == 0 & $stts == 1) {
					$html += '<td><a onclick="updateDB('+"'"+$parseAr[$i]['id'].toString()+"', 'CMOETIME')"+'"'+'>来た</a></td>';
					// $html += "<td><a onload=\"updateDB('"+$parseAr[$i]['id'].toString()+"', 'CMOETIME')\">来た</a></td>";
				} else {
					$html += "<td>"+ $parseAr[$i]['cometime'].toString() +"</td>";
				}
				if ($parseAr[$i]['finishtime'].toString().length == 0 & $parseAr[$i]['cometime'].toString().length == 8 & $stts == 1) {
					$html += '<td><a onclick="updateDB('+"'"+$parseAr[$i]['id'].toString()+"', 'FINISHTIME')"+'"'+'>終わった</a></td>';
					// $html += "<td><a onload=\"updateDB('"+$parseAr[$i]['id'].toString()+"', 'FINISHTIME')\">終わった</a></td>";
				} else {
					$html += "<td>"+ $parseAr[$i]['finishtime'].toString() +"</td>";
				}
				$html += "</tr>";
			}
			$html += "</table>";

			$('div#makeTable').html($html);
		}
	});
}

function insertDB() {
	$.ajax({
		url: "./yoyaku.php",
		type: "POST",
		data: {
			CG: $('input#CG').val(),
			NUM: $('input#NUM').val()
		}
	});
}

function updateDB( $id, $column ) {
	console.log("updateDB column="+$column);
	$.ajax({
		url: "./updateDB.php?id="+location.href[location.href.length-1],
		type: "POST",
		data: {
			ID: $id,
			COLUMN: $column
		}
	});
	
}

function deletDB() {
	$.ajax({
		url: "./deletDB.php",
		type: "POST"
	});
}