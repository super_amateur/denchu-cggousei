<!-- 
GET men:
	camera
	batch
	fguid
 -->
 <!--  -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Content-Style-Type" content="text/css">
		<!-- スマホ対応のため、スマホの画面解像度に合わせた拡大を実現、画面を横にした時にスケールを縮小しないようにする。 -->
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<!-- スマホでホーム画面にブックマークした場合にフルスクリーンで起動できるようになる。 -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<title>確認しすてみゅ</title>
		<meta name="version" content="0.3">
		<meta name="last update" content="2016/04/13">
		<meta name="author" content="どどど素人">
		<link rel="stylesheet" type="text/css" href="style.css">
		<script type="text/javascript" src="script/jquery-2.2.1.min.js"></script>
		<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.12.2.min.js"></script> -->
		<script type="text/javascript" src="script/script.js"></script>
		<script type="text/javascript">

		SelectNm = -1; // int 選択中のid番号

		debug = true; // デバグで停止時はこれをfalse。

		count = 0;

		$(function() {
			makeTable(true);
			if (<?php print_r(($_GET['men']=='guid'? 'false': 'true')); ?>) {
				// guid men じゃなかったら
				$('#kakunin_submit_button').on('click', function(event) {
					// console.log("click button.");
					updateDB2('<?php echo $_GET['men'] ?>', SelectNm);
					SelectNm = -1;
				});
			} else {
				// guid men だったら

			}
		});

		function guidClick(id) {
			// $.ajax({
			// 	url: "./php/colorSetTest.php",
			// 	type: "GET",
			// 	data: {
			// 		id: SelectNm,
			// 		color: id
			// 	},
			// 	success: function ( arr ) {
			// 		console.log("colorSetTest "+arr);
			// 		SelectNm = -1;
			// 	}
			// });
			
			updateDB2((id==1? 'guid': 'unguid'), SelectNm);
			SelectNm = -1;
		}
			
function makeTable( next ) {
	count++;
	// console.log("makeTable() : "+count);
	men = '<?php print_r($_GET['men']) ?>';
	$.ajax({
		url: "./php/lookDB.php",
		type: "POST",
		// data: { val : $keyword }, // 検索など引数を渡す必要があるときこれを使う。受け取りは$_POST['val']的な。
		// dataType: 'json',        // サーバーなどの環境によってこのオプションが必要なときがある
		success: function( $arr ){

			var $parseAr = JSON.parse( $arr );
			var color = new Array('#ffffff', '#ffdddd', '#ddffdd', '#ddddff', '#ffffdd', '#ffddff', '#ddffff');
			// var $parseAr = $arr;

			// var $id = $parseAr[0]['id'].toString();
			$html = "<table><tr><th>整理券</th><th>CG</th><th>人数</th><th>状態</th><th>ID</th></tr>";
			// $html = "<table><tr><th></th><th>CG</th><th>人数</th><th>状態</th><th>受付</th><th>来た</th><th>撮影終了</th><th>受渡</th></tr>";
			for (var $i = $parseAr.length-1; $i >= 0; $i--) {
				var bgColor = color[($parseAr[$i]['color'].toString()-0)];
				$html += '<tr class="table_info_tr" style="color:'+($parseAr[$i]['status'].toString()=='終了'? 'gray': 'black')+';" onclick="$(\'.table_info_tr\').css(\'color\',\'black\'); SelectNm='+$i+'; $(this).css(\'color\', \'red\');">';
				if ($parseAr[$i]['status'].toString()=='終了') continue;
				$html += "<td>"+ $parseAr[$i]['chicket'].toString() +"</td>";
				// $html += "<td>"+ $parseAr[$i]['id'].toString() +"</td>";
				$html += "<td>"+ $parseAr[$i]['cg'].toString() +"</td>";
				$html += "<td>"+ $parseAr[$i]['num'].toString() +"</td>";
				$html += '<td style="background-color:'+bgColor+';">'+ $parseAr[$i]['status'].toString() +"</td>";
				$html += "<td>"+ $parseAr[$i]['id'].toString() +"</td>";
				// $html += "<td>"+ $parseAr[$i]['recepttime'].toString() +"</td>";
				// $html += "<td>"+ $parseAr[$i]['cometime'].toString() +"</td>";
				// $html += "<td>"+ $parseAr[$i]['finishtime'].toString() +"</td>";
				// $html += "<td>"+ $parseAr[$i]['batchtime'].toString() +"</td>";
				$html += "</tr>";
			}

			if (SelectNm != -1) {
				if (men == 'guid') if ($parseAr[SelectNm]['status'] != '待機中') count++;
				if (men == 'camera') if ($parseAr[SelectNm]['status'] != '撮影中') count++;
				if (men == 'batch') if ($parseAr[SelectNm]['status'] != 'バッチ待ち') count++;
			}
			if (count == 3000) {
				SelectNm = -1;
				count = 0;
			}
			// if (count%100==0 & SelectNm != -1) {
			// 	if (men == 'guid') if ($parseAr[SelectNm]['status'] != '待機中') SelectNm = -1;
			// 	if (men == 'camera') if ($parseAr[SelectNm]['status'] != '撮影中') SelectNm = -1;
			// 	if (men == 'batch') if ($parseAr[SelectNm]['status'] != 'バッチ待ち') SelectNm = -1;
			// }

			$html += "</table>";

			$('div#makeTable').html($html);

			if (SelectNm <0) {
				searchStatus = '';
				if (men=='camera') searchStatus = '撮影中';
				if (men=='batch') searchStatus = 'バッチ待ち';
				if (men=='guid') searchStatus = '待機中';
				for (var i = 0; i < $parseAr.length; i++) {
					// console.log($parseAr[i]['status'].toString());
					// console.log($parseAr[i]['status'].toString() == "撮影中");
					if ($parseAr[i]['status'].toString().search(searchStatus) != -1) {
						SelectNm = i;
						$($("div#makeTable table tr")[$parseAr.length -SelectNm]).css('color', 'red');
						$(window).scrollTop($($('#makeTable table tr')[$parseAr.length -SelectNm]).position().top - ($(window).height()/10));
						break;
					}
				}
			} else {
				$($("div#makeTable table tr")[$parseAr.length -SelectNm]).css('color', 'red');
			}

			if (next & debug) {
				makeTable(true);
			}
		},
		error: function ( arr ) {
			setTimeout('makeTable(true)', 5000);
		}
	});
}
		</script>

		<link rel="apple-touch-icon" sizes="57x57" href="icon/apple-touch-icons/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="60x60" href="icon/apple-touch-icons/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="icon/apple-touch-icons/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="icon/apple-touch-icons/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="icon/apple-touch-icons/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="icon/apple-touch-icons/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="icon/apple-touch-icons/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="icon/apple-touch-icons/apple-touch-icon-152x152.png">
		
		<link rel="shortcut icon" href="./favicon.ico" type="image/vnd.microsoft.icon" />
		<link rel="icon" href="./favicon.ico" type="image/vnd.microsoft.icon" />
	</head>
	<body>
	<!-- <body onload="$interval = setInterval( 'makeTable(false)', 3000);"> -->
		<div id="main">
			<div id="makeTable"></div>
			<a href="">リロード</a>
			<div style="height:60%;"> </div>
		</div>
		<div id="kakunin_submit_button" style="position:fixed; width:100%; height:40%; bottom:0px; border-radius:5%; background:green; text-align:center; color:white;">
			<div class="center_center" style="font-size:60px; height:100%">
				<?php
					if ($_GET['men']=='camera') print_r('撮影終了');
					if ($_GET['men']=='batch') print_r('受渡完了');
					// if ($_GET['men']=='guid') print_r('案内');
					// if ($_GET['men']=='guid') {
					// 	$html = '
					// 	<a style="background-color:#ff4400;" onclick="guidClick(1);">案内A</a>
					// 	<a style="" onclick="guidClick(3);">案内B</a>
					// 	';
					// 	print_r($html);
					// }
					if ($_GET['men']=='guid') {
						$html = '
						<a style="background-color:#ff4400;border:3px solid white;" onclick="guidClick(1);">案内</a>
						<a style="border:3px solid white;" onclick="guidClick(3);">取消</a>
						';
						print_r($html);
					}
				?>
			</div>
		</div>
		<div id="copyright">copyright 2016 Kazuki FUKUNAGA with 電chu!</div>
	</body>
</html>
