<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Content-Style-Type" content="text/css">
		<!-- スマホ対応のため、スマホの画面解像度に合わせた拡大を実現、画面を横にした時にスケールを縮小しないようにする。 -->
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title> </title>
		<meta name="version" content="0.3">
		<meta name="last update" content="2016/04/13">
		<meta name="author" content="どどど素人">
		<link rel="stylesheet" type="text/css" href="style.css">
		<script type="text/javascript" src="script/jquery-2.2.1.min.js"></script>
		<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.12.2.min.js"></script> -->
		<script type="text/javascript" src="script/script.js"></script>
	</head>
	<body onload="$interval = setInterval( 'makeTable(1)', 2000 );">
	<?php
		if ($_GET['id'] == '') {
			echo '<div id="main" hidden>';
		} else {
			echo '<div id="main" style="background-color:'.array(
				"0" => "#ffdddd", //赤
				"1" => "#ffffdd", //黄
				"2" => "#ddffdd", //緑
			)[$_GET['id']].'" onload="$('."'#selection'".').css('."'display', 'none'".');">';
		}
	?>
			<div id="makeTable"></div>
		</div>
		<div id="selection" hidden>
			<select id="select" multiple>
				<option value="0">Red</option>
				<option value="1">Yellow</option>
				<option value="2">Green</option>
			</select>
			<a onclick="location.href = location.href+'?id='+$('#select').val();">ID決定</a>
		</div>
		<div id="copyright">copyright 2016 Kazuki FUKUNAGA with 電chu!</div>
	</body>
</html>
